/**
 * @param  {(counter:number)=>boolean} test test if the wrapped function should
 *    be called, test receives a counter which has a initial value 0 and
 *    will be incremented each time when the wrapping function is called
 * @param  {} useLastReturn=false if return the last result when test failed
 * @returns {(...args: T) => U} a function with the identical signature with the wrapped
 *    function, you use this returned function as the wrapped function, but the
 *    returned function will only be called  when test passed,
 *    otherwise, the wrapped function won't be called, and **null** or last return
 *    will be used as the return value.
 */
function fireWhen<T extends any[], U>(
  test: (counter: number) => boolean,
  useLastReturn = false
) {
  let counter = 0;
  let result: U | null = null;
  return (wrapped: (...args: T) => U) => (...args: T) => {
    if (test(counter++)) {
      result = wrapped(...args);
      return result;
    } else {
      return useLastReturn ? result : null;
    }
  };
}

export default fireWhen;
