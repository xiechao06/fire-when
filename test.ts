import fireWhen from "./index";

test("fire each 2 times", () => {
  const wrapped = jest.fn((i: number) => i);
  const f = fireWhen(i => i % 2 === 0)(wrapped);
  expect(f(1)).toEqual(1);
  expect(wrapped).toBeCalledTimes(1);
  expect(f(2)).toBe(null);
  expect(wrapped).toBeCalledTimes(1);
  expect(f(3)).toEqual(3);
  expect(wrapped).toBeCalledTimes(2);
});

test("use last return", () => {
  const wrapped = jest.fn((i: number) => i);
  const f = fireWhen(i => i % 2 === 0, true)(wrapped);
  expect(f(1)).toEqual(1);
  expect(wrapped).toBeCalledTimes(1);
  expect(f(2)).toEqual(1);
  expect(wrapped).toBeCalledTimes(1);
  expect(f(3)).toEqual(3);
  expect(wrapped).toBeCalledTimes(2);
});
