# fire-when

[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=flat-square)](http://opensource.org/licenses/MIT)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![manpm](https://img.shields.io/badge/manpm-compatible-3399ff.svg)](https://github.com/bahmutov/manpm)
[![npm](https://img.shields.io/npm/v/fire-when.svg?style=flat-square)](https://www.npmjs.org/package/fire-when)

Call a function when a certain criterion is met.

[Documentation](https://xiechao06.gitlab.io/fire-when).

## Why

To reduce the function calls. for example, in a react application, if some state mutations
are expensive and repeat at very high frequency, you could use this package to
reduce the state mutations, if dropping some state mutation doesn't matter.

## Installation

```bash
npm i fire-when
```

## Quick start

```javascript

const wrappedF = arg => arg
// realF will will called when f is called 3N times.
const wrappingF = fireWhen(counter => counter % 3 === 0)(wrappedF)

wrappingF('foo') // output foo
wrappingF('foo') // output undefined
wrappingF('foo') // output undefined
wrappingF('foo') // output foo
```

## A more complex but somewhat ridiculous example

```javascript
import React from 'react';
import fireWhen from 'fire-when';

class App extends React.Component {
  state = {
    ts: Date.now(),
  }

  componentDidMount() {
    // update ts every 10ms? 10 * 100ms actually
    setInterval(this.updateTs, 10);
  }

  updateTs = fireWhen(i => i % 100 === 0)(() => {
    this.setState(({ts}) => ({
      ts: Date.now()
    }));
  })

  render() {
    return (<div>{this.state.ts}</div>);
  }
}

```
