"use strict";
exports.__esModule = true;
/**
 * @param  {(counter:number)=>boolean} test test if the wrapped function should
 *    be called, test receives a counter which has a initial value 0 and
 *    will be incremented each time when the wrapping function is called
 * @param  {} useLastReturn=false if return the last result when test failed
 * @returns {(...args: T) => U} a function with the identical signature with the wrapped
 *    function, you use this returned function as the wrapped function, but the
 *    returned function will only be called  when test passed,
 *    otherwise, the wrapped function won't be called, and **null** or last return
 *    will be used as the return value.
 */
function fireWhen(test, useLastReturn) {
    if (useLastReturn === void 0) { useLastReturn = false; }
    var counter = 0;
    var result = null;
    return function (wrapped) { return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (test(counter++)) {
            result = wrapped.apply(void 0, args);
            return result;
        }
        else {
            return useLastReturn ? result : null;
        }
    }; };
}
exports["default"] = fireWhen;
